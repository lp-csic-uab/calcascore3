# exWx/setup.py
#
## python setup.py py2exe 
#
from distutils.core import setup
import py2exe

setup(
    windows = [
              {'script': "calcascore_main_10.pyw",
              'icon_resources':[(0,'img//calcascore48.ico')],
              'dest_base' : "calcascore_06",    
              'version' : "0.6",
              'company_name' : "JoaquinAbian",
              'copyright' : "No Copyrights",
              'name' : "CalcAscore"}
              ],
            
    options = {
              'py2exe': {
                        'packages' :    [],
                        'includes':     [],
                        'excludes':     ['EasyDialogs','matplotlib',
                                         'bsddb', 'curses', 'Pyrex',
                                         'mpl_toolkits', 'PyQt4',
                                         'tcl', 'Tkconstants', 'Tkinter',
                                         '_gtkagg', '_tkagg', 'pywin.debugger',
                                         'pywin.debugger.dbgcon', 'pywin.dialogs',
                                         'email', 'testing', 'fft'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll',
                                         'tk84.dll',
                                         'tcl84.dll',
                                         'libgdk_pixbuf-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    data_files = []
                 
    )
