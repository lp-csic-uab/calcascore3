
---

**WARNING!**: This is the *Old* source-code repository for CalcAscore3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/calcascore3/) located at https://sourceforge.net/p/lp-csic-uab/calcascore3/**  

---  
  
  
# CalcAscore3 program

Python 3 version of CalcAscore

---

**WARNING!**: This is the *Old* source-code repository for CalcAscore3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/calcascore3/) located at https://sourceforge.net/p/lp-csic-uab/calcascore3/**  

---  
  