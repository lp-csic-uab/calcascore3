import wx
import math
import base64
from io import BytesIO
from scipy.stats import binom
from calcascore_common import ICON
from calcascore_cls_baseframe import MyAscore

definicion = dict(HIT="Numero de aciertos, n",
                  TRIALS="Numero maximo de aciertos. Iones diana, N",
                  LEVEL="Numero de iones testados que produce maxima diferencia",
                  ION="Numero de  iones por sector",
                  BIN="Probabilidad binomial acumulada"
                  )


# noinspection PyUnusedLocal
class Ascore(MyAscore):
    
    mapclass = dict(
                    hit_1=("tc_hit1", "lb21", "0", definicion['HIT']),
                    hit_2=("tc_hit2", "lb22", "0", definicion['HIT']),
                    trials=("tc_tr", "lb4", "0", definicion['TRIALS']),
                    level=("tc_lv", "lb31", "0", definicion['LEVEL']),
                    ion=("tc_io", "lb32",  "0", definicion['ION']),
                    bin1=("tc_bp1", "lb71", "0", definicion['BIN']),
                    bin2=("tc_bp2", "lb72", "0", definicion['BIN'])
                    )
    
    def __init__(self, *args, **kwds):
        MyAscore.__init__(self, *args, **kwds)
        
        self.initparams()
        self.prob = 0
        self.score1 = 0
        self.score2 = 0
        self.ascore = 0
        self.auto = 0

        self.hit1 = None
        self.hit2 = None
        self.bin1 = None
        self.bin2 = None
        self.trials = None
        self.ion = None
        self.level = None
        
        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)
        self.Bind(wx.EVT_TEXT, self.on_level, self.tc_lv)
        self.Bind(wx.EVT_TEXT, self.on_ion, self.tc_io)
        self.Bind(wx.EVT_CHECKBOX, self.on_auto, self.chk_auto)
    
        self.set_icon(ICON)
       
    def set_icon(self, ico):
        """ """
        data = base64.decodebytes(bytes(ico, 'utf8'))
        flhndl = BytesIO(data)
        img = wx.Image(flhndl, wx.BITMAP_TYPE_ANY)
        _icon = wx.Icon(wx.Bitmap(img))
        self.SetIcon(_icon)
    
    def initparams(self):
        for item, value in Ascore.mapclass.items():
            setattr(self, item, float(value[2]))
            lbobj = getattr(self, value[1])
            # txobj = getattr(self, value[0])
            lbobj.SetToolTip(value[3])
            # txobj.SetToolTipString(value[3])
            # txobj.SetValue(value[2])
                    
    def score(self, hits):
        """
        calcula la binomial - binom.pmf(hits, trials, prob)-
        acumulada y el score.
        """
        #
        cbin = sum(binom.pmf(range(hits, self.trials+1), self.trials, self.prob))
        #
        # print self.prob, cbin
        scr = -10 * math.log(cbin, 10)
        return cbin, scr
    
    def on_level(self, evt):
        self.level = int(behave(self.tc_lv.GetValue()))
        self.calc_prob()
        
    def on_ion(self, evt):
        self.ion = int(behave(self.tc_io.GetValue()))
        self.calc_prob()
    
    def on_auto(self, evt):
        self.calc_prob()
        
    def calc_prob(self):
        if self.chk_auto.IsChecked():
            self.level = int(behave(self.tc_lv.GetValue()))
            self.ion = int(behave(self.tc_io.GetValue()))
            try:
                self.prob = float(self.level)/self.ion
                self.tc_pr.SetValue(str(self.prob))
            except ValueError:
                pass
            except ZeroDivisionError:
                self.tc_pr.SetValue('-')
    
    def on_run(self, evt):
        """ """
        self.hit1 = int(behave(self.tc_hit1.GetValue()))
        self.hit2 = int(behave(self.tc_hit2.GetValue()))
        self.trials = int(behave(self.tc_tr.GetValue()))
        self.prob = float(behave(self.tc_pr.GetValue()))
        
        if self.hit1 > self.trials or self.hit1 > self.trials:
            aviso("Numero de hits n no puede ser superior al de trials N")
            self.tc_asc.SetValue("")
            return
        
        if self.prob > 1:
            aviso("El valor de probabilidad no puede ser mayor de 1")
            return
            
        try:
            self.bin1, self.score1 = self.score(self.hit1)
            self.bin2, self.score2 = self.score(self.hit2)
        except OverflowError:
            aviso("Falta un dato")
            return
        
        self.ascore = self.score1 - self.score2
        
        self.tc_bp1.SetValue("%.2e" % self.bin1)
        self.tc_bp2.SetValue("%.2e" % self.bin2)
        self.tc_sc1.SetValue("%.2f" % self.score1)
        self.tc_sc2.SetValue("%.2f" % self.score2)
        self.tc_asc.SetValue("%.2f" % self.ascore)
        
        
def behave(entry):
    try:
        float(entry)
        return entry
    except ValueError:
        return 0

def aviso(warning):
    dlg = wx.MessageDialog(None, warning, style=wx.OK)
    if dlg.ShowModal() == wx.ID_OK:
        dlg.Destroy()


if __name__ == "__main__":
    app = wx.App(redirect=0)
    pscore = Ascore(None, -1, "")
    pscore.Show()
    app.MainLoop()
